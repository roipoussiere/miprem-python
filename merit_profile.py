import json
import os.path as op
import re
from math import floor, ceil
from typing import List, Dict, TextIO

import lxml.etree as et
from cairosvg import svg2png
from colorclass import Color
from terminaltables import SingleTable

PROJECT_PATH = op.dirname(op.abspath(__file__))


class MeritProfile:
    """Represents a merit profile"""

    def __init__(self, input_data: TextIO):
        mp = json.loads(input_data.read())
        if '_question' not in mp:
            raise ValueError('Question not found in input data.')
        self.question: str = mp['_question']

        if '_mentions' not in mp:
            raise ValueError('Mentions not found in input data.')
        self.mentions: List[str] = mp['_mentions']

        [mp.pop(meta_key) for meta_key in ['_mentions', '_question']]
        if len(mp) < 2:
            raise ValueError('There must be at least 2 mentions.')
        if len(mp) > 20:
            raise ValueError('There must be at most 20 mentions.')
        self.proposals_scores: Dict[str, List[int]] = mp
        self.proposals: List[str] = mp.keys()
        self.nb_proposals: int = len(self.proposals)

        if not self.proposals_scores:
            raise ValueError('There is no proposal.')
        scores_sums = [sum(scores) for scores in self.proposals_scores.values()]
        if not all(elem == scores_sums[0] for elem in scores_sums):
            sums = ['%s: %d' % (c, scores_sums[i]) for i, c in enumerate(self.proposals_scores.keys())]
            raise ValueError('The scores sum are not equal for each proposal: %s' % ', '.join([str(s) for s in sums]))

    def __str__(self) -> str:
        str_mj = 'question: %s\n' % self.question
        str_mj += 'mentions: %s\n' % ', '.join(self.mentions)
        str_mj += '\n'.join(['%s: %s' % (name, ', '.join([str(score) for score in scores]))
                             for (name, scores) in self.proposals_scores.items()])
        return str_mj

    def build_png(self, width: int, height: int, sidebar_width: int, user_css: str) -> bytes:
        svg = self.build_svg(width, height, sidebar_width, user_css)
        return svg2png(bytestring=svg)

    def term_table(self) -> str:
        data = [['proposal'] + self.mentions]
        for proposal, scores in self.proposals_scores.items():
            proposal_row = [Color('{blue}%s{/blue}' % proposal).value_colors]
            for i, score in enumerate(scores):
                if floor((i+1) / len(self.mentions) * 3) == 0:
                    proposal_row += [Color('{red}%s{/red}' % str(score)).value_colors]
                elif ceil(i / len(self.mentions) * 3) == 3:
                    proposal_row += [Color('{green}%s{/green}' % str(score)).value_colors]
                else:
                    proposal_row += [Color('{yellow}%s{/yellow}' % str(score)).value_colors]
            data.append(proposal_row)
        table = SingleTable(data, title=Color('  {cyan}%s{/cyan}  ' % self.question).value_colors)
        return table.table
