import json
import os.path as op
import re
from math import floor, ceil
from typing import List, Dict, TextIO

from cairosvg import svg2png
from colorclass import Color
from terminaltables import SingleTable
from pybars import Compiler

SRC_PATH = op.dirname(op.abspath(__file__))

class Renderer:
    TPL_MERIT_PROFILE = op.join(SRC_PATH, 'templates', 'merit_profile.svg')
    TPL_TEST_SUITE = op.join(SRC_PATH, 'templates', 'test_suite.csv')

    def __init__(self, options, css, template):
        self.options = options
        self.css = css
        self.template = template

    def render_svg(self, data) -> str:
        compiler = Compiler()
        compiled_template = compiler.compile(self.template)

        helpers = {
            'add': lambda this, a, b : a + b,
            'sub': lambda this, a, b : a - b,
            'mul': lambda this, a, b : a * b,
            'div': lambda this, a, b : a / b,
            'str': lambda this, array : '[' + ', '.join(array) + ']',
            'len': lambda this, array : len(array),
            'get': lambda this, dict, i : dict[i],
            'sum': lambda this, array : sum(array),
            'slice': lambda this, array, start, end : array[start:end],
            'check': lambda this, data : False
        }

        return compiled_template(data, helpers=helpers)

    def render_png(self, data) -> bytes:
        return svg2png(bytestring=self.render_svg(data))

    def term_table(self, data) -> str:
        # data = [['proposal'] + self.mentions]
        for proposal, scores in self.proposals_scores.items():
            proposal_row = [Color('{blue}%s{/blue}' % proposal).value_colors]
            for i, score in enumerate(scores):
                if floor((i+1) / len(self.mentions) * 3) == 0:
                    proposal_row += [Color('{red}%s{/red}' % str(score)).value_colors]
                elif ceil(i / len(self.mentions) * 3) == 3:
                    proposal_row += [Color('{green}%s{/green}' % str(score)).value_colors]
                else:
                    proposal_row += [Color('{yellow}%s{/yellow}' % str(score)).value_colors]
            data.append(proposal_row)
        table = SingleTable(data, title=Color('  {cyan}%s{/cyan}  ' % self.question).value_colors)
        return table.table
