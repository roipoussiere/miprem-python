# Miprem - Python implementation

The Python implementation of [Miprem](https://roipoussiere.frama.io/miprem/).

## Features

- can be used as a Python library;
- display input data as a table from the CLI;
- in addition to CLI options, users can add CSS styling on any element of the image.

## Installation

```
pip install miprem
```

## Usage

### Display a merit profile

```
miprem sample.mp.json
```

The image is generated and opened in your default image viewer.

### Display merit profile data as table

```
miprem sample.mp.json --table
```

![](doc/mp_table.jpg)

Other options can be listed with `miprem --help`.

### Export the figure

As svg:

```
miprem sample.mp.json --svg doc/mp_figure.svg
```

As png:

```
miprem sample.mp.json --png doc/mp_figure.png
```

## Installation for development

### Install Poetry

[Poetry](https://python-poetry.org/) is used to resolve dependencies, as well as to create and publish package and must
be [installed](https://python-poetry.org/docs/#installation).

### Install Miprem dependencies

```
poetry install
```

### Run Miprem development version

```
poetry run miprem
```

## Developing

### Python interpreter

You may select the Python interpreter for your IDE, you can find it with:

```
ls $(poetry config settings.virtualenvs.path | tr -d '"')/miprem-*/bin/python3
```

### Update package

```
poetry build
```
